﻿using System;
using Gtk;
using System.Drawing.Imaging;
using System.Drawing;
using DICOMTools;
using System.Collections.Generic;
using Cairo;
using Gdk;
using System.IO;
using System.Runtime.InteropServices;

using DICOMViewer;

public partial class MainWindow: Gtk.Window
{
	private string selectedSlice = "";
	private Pixbuf frame = null;
	private Pixbuf processedFrame = null;
	private DicomDir currentDicomDir = null;
	private SegmentationGrid gridModel = null;
	private Int32 selectedEntry = -1;

	public MainWindow () : base (Gtk.WindowType.Toplevel)
	{
		Build ();
		statusBar.Push (statusBar.GetContextId(""), "Waiting...");
		InitTreeView ();
		InitSegmentationPanel ();
	}

	void InitTreeView ()
	{	
		Gtk.TreeViewColumn sliceColumn = new Gtk.TreeViewColumn ();
		sliceColumn.Title = "Series Set";

		// Create the text cell that will display the slice path	
		Gtk.CellRendererText sliceCell = new Gtk.CellRendererText ();
		sliceColumn.PackStart (sliceCell, true);
		sliceColumn.AddAttribute (sliceCell, "text", 0);

		tree.AppendColumn (sliceColumn);

		var studySeriesList = new Gtk.TreeStore (typeof(string));

		tree.Model = studySeriesList;

		tree.CursorChanged+= new EventHandler(tree_CursorChanged);
	}

	void InitSegmentationPanel(){	

		gridModel = new SegmentationGrid (segmentationTree);

		segmentationTree.Model = gridModel.Model;
	}
		
	protected void OnDeleteEvent (object sender, DeleteEventArgs a)
	{
		Application.Quit ();
		a.RetVal = true;
	}			

	protected void OnExitActionActivated (object sender, EventArgs e)
	{
		Application.Quit();
	}

	protected void OnProcessActionActivated (object sender, EventArgs e)
	{
		DicomProcessor.testProc ();
	}

	protected void OnOpenDICOMDIRActionActivated (object sender, EventArgs e)
	{
		Gtk.FileChooserDialog filechooser =
			new Gtk.FileChooserDialog("Choose the file to open",
				this,
				FileChooserAction.Open,
				"Cancel",ResponseType.Cancel,
				"Open",ResponseType.Accept);

		if (filechooser.Run() == (int)ResponseType.Accept) 
		{
			System.IO.FileStream file = System.IO.File.OpenRead(filechooser.Filename);
			currentDicomDir = DicomProcessor.ParseDICOMDIR (filechooser.Filename);
			//StatusBar
			statusBar.Push (statusBar.GetContextId(""), currentDicomDir.ToString() );

			//Load TreeList
			LoadSeriesTree (currentDicomDir.GetSeries ());

			Console.WriteLine(currentDicomDir.ParentPath);

			file.Close();
		}

		filechooser.Destroy();
	}		
		
	void LoadSeriesTree(List<SerieDicom> series){
		var studySeriesList = (Gtk.TreeStore)tree.Model;
		foreach (var serie in series) {
			Gtk.TreeIter iter = studySeriesList.AppendValues(serie.Id);
			foreach (var path in serie.Images) {
				studySeriesList.AppendValues (iter, path);
			}
		}

	}

	void tree_CursorChanged(object sender, EventArgs e){
		TreeSelection selection = (sender as TreeView).Selection;
		TreeModel model;
		TreeIter iter;

		// THE ITER WILL POINT TO THE SELECTED ROW
		if(selection.GetSelected(out model, out iter)){		
			var value = model.GetValue (iter, 0).ToString ();
			//Check if value is an image path
			if (value.Contains("\\")) {
				Console.WriteLine ("Selected Value:" + value);		
				DirectoryInfo dir = new DirectoryInfo (currentDicomDir.ParentPath);
				selectedSlice = System.IO.Path.Combine(currentDicomDir.ParentPath, value);
				Console.WriteLine (selectedSlice);
				var bitmap = DicomProcessor.RenderDicom (selectedSlice);
				var image = BitmapToImage (bitmap);
				processedFrame = frame = ImageToPixbuf (image);

				drawingArea.ExposeEvent+= updateGraphics ;
				drawingArea.QueueDraw ();
			}
		}	
	}

	void updateGraphics (object sender, EventArgs e)
	{	
		var area = (DrawingArea)sender;

		using (Cairo.Context ct = Gdk.CairoHelper.Create (area.GdkWindow)) {

			Gdk.GC gc=new Gdk.GC(area.GdkWindow);
			gc.RgbFgColor=new Gdk.Color(255,50,50);
			gc.RgbBgColor=new Gdk.Color(0,0,0);
			gc.SetLineAttributes(3,LineStyle.OnOffDash,CapStyle.Projecting,JoinStyle.Round);				

			area.GdkWindow.DrawPixbuf(gc, processedFrame, 0, 0, 0, 0, processedFrame.Width, processedFrame.Height, RgbDither.Normal, 0, 0);

			ct.Save ();

			((IDisposable) ct.GetTarget()).Dispose ();
		}

	}		

	public static System.Drawing.Image BitmapToImage(Bitmap img)
	{
		using (MemoryStream stream = new MemoryStream())
		{
			img.Save(stream, System.Drawing.Imaging.ImageFormat.Png);
			var result = System.Drawing.Image.FromStream (stream);
			stream.Close();
			return result;
		}
	}

	private void onAdd_click (object sender, EventArgs e)
	{
		this.gridModel.AddNewEntry ();
	}

	private void onDelete_click (object sender, EventArgs e)
	{
		if (selectedEntry >= 0) {
			this.gridModel.DeleteEntry (selectedEntry);
		}
	}

	protected void OnSegmentationTreeCursorChanged (object sender, EventArgs e)
	{
		TreeSelection selection = (sender as TreeView).Selection;
		TreeModel model;
		TreeIter iter;

		// THE ITER WILL POINT TO THE SELECTED ROW
		if (selection.GetSelected (out model, out iter)) {		
			//var value = model.GetValue (iter, 0).ToString ();
			TreePath path = model.GetPath (iter);
			selectedEntry = path.Indices [0];
		}
	}

	protected void OnClearActionActivated (object sender, EventArgs e)
	{
		this.gridModel.ClearAll ();
	}

	private Gdk.Pixbuf ImageToPixbuf (System.Drawing.Image image) 
	{ 
		using (System.IO.MemoryStream stream = new System.IO.MemoryStream ()) { 
			image.Save (stream, System.Drawing.Imaging.ImageFormat.Bmp); 
			stream.Position = 0; 
			return new Gdk.Pixbuf (stream); 
		} 
	}		

	//Process SegmentationRecords
	protected void OnMediaPlayActionActivated (object sender, EventArgs e)
	{
		if (!string.IsNullOrEmpty (selectedSlice)) 
		{
			var bitmap = DicomProcessor.RenderDicom (selectedSlice, gridModel.lista);
			var image = BitmapToImage (bitmap);
			processedFrame = ImageToPixbuf (image);
			drawingArea.QueueDraw ();
		}

	}

}

	


