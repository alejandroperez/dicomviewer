﻿using System;
using Gtk;
using System.Collections.Generic;
using DICOMTools;

namespace DICOMViewer
{
	public class SegmentationGrid
	{

		public ListStore Model { get; set;}
		public List<SegmentationRecord> lista { get; set;}

		public SegmentationGrid (TreeView tv)
		{
			CreateSampleData (this.Model);
			this.Model = SetupModel (tv);

			RefreshModel ();
				
			tv.RulesHint = true;
			tv.Selection.Mode = SelectionMode.Single;

		}

		private ListStore SetupModel( TreeView tv ){
			var m = new ListStore(typeof(Boolean),typeof(int),typeof(int),typeof(string));

			var toggleCell = new CellRendererToggle ();
			toggleCell.Activatable = true;
			toggleCell.Toggled += activeCell_Toggled;
			var selectedCol = new TreeViewColumn( "Active", 
				toggleCell, "active", (int) Column.Enabled);			
			
			tv.AppendColumn( selectedCol );

			var fromCell = new CellRendererText ();
			fromCell.Editable = true;
			fromCell.Edited += fromCell_Edited;
			var fromCol = new TreeViewColumn( "From", 
				fromCell, "text", (int) Column.HFrom );			
			tv.AppendColumn( fromCol );

			var toCell = new CellRendererText ();
			toCell.Editable = true;
			toCell.Edited += toCell_Edited;
			var toCol = new TreeViewColumn( "To", 
				toCell, "text", (int) Column.HTo );
			tv.AppendColumn( toCol );

			var colorCell = new CellRendererText ();
			colorCell.Editable = true;
			colorCell.Edited += colorCell_Edited;
			var colorCol = new TreeViewColumn( "Color", 
				colorCell, "text", (int) Column.Color );
			tv.AppendColumn( colorCol );

			tv.Model = m;

			return m;
		}

		void CreateSampleData( ListStore model ) {
			SegmentationRecord foo;
			lista = new List<SegmentationRecord> ();

			foo = new SegmentationRecord () {
				Enabled = true, HFrom = 100, HTo = 200, Color = "#f0C332"
			};
			lista.Add(foo);

			foo = new SegmentationRecord () {
				Enabled = false, HFrom = 500, HTo = 1000, Color = "#3210C3"
			};
			lista.Add(foo);

			foo = new SegmentationRecord () {
				Enabled = false, HFrom = 1001, HTo = 1500, Color = "#C3f032"
			};
			lista.Add(foo);

			foo = new SegmentationRecord () {
				Enabled = false, HFrom = -1000, HTo = 100, Color = "#2121f1"
			};
			lista.Add(foo);

		}

		void AddValue(SegmentationRecord record){
			Model.AppendValues( record.Enabled, record.HFrom, record.HTo, record.Color);
		}

		private static void activateSegmentation(object o, Gtk.ToggledArgs args)
		{
			var cell =(CellRendererToggle)o;		
		}

		private void activeCell_Toggled (object o, Gtk.ToggledArgs args)
		{

			TreePath path = new TreePath (args.Path);
			TreeIter iter;
			Model.GetIter (out iter, path);
			int i = path.Indices[0];

			SegmentationRecord foo;
			try {
				foo = lista[i];
				foo.Enabled = !foo.Enabled;
			} catch (Exception e) {
				Console.WriteLine (e);
				return;
			}

			Model.SetValue (iter, (int) Column.Enabled, foo.Enabled);
		}	

		private void fromCell_Edited (object o, Gtk.EditedArgs args)
		{

			TreePath path = new TreePath (args.Path);
			TreeIter iter;
			Model.GetIter (out iter, path);
			int i = path.Indices[0];

			SegmentationRecord foo;
			try {
				foo = lista[i];
				foo.HFrom = int.Parse (args.NewText);
			} catch (Exception e) {
				Console.WriteLine (e);
				return;
			}

			Model.SetValue (iter, (int) Column.HFrom, foo.HFrom);
		}	

		private void toCell_Edited (object o, Gtk.EditedArgs args)
		{

			TreePath path = new TreePath (args.Path);
			TreeIter iter;
			Model.GetIter (out iter, path);
			int i = path.Indices[0];

			SegmentationRecord foo;
			try {
				foo = lista[i];
				foo.HTo = int.Parse (args.NewText);
			} catch (Exception e) {
				Console.WriteLine (e);
				return;
			}

			Model.SetValue (iter, (int) Column.HTo, foo.HTo);
		}

		private void colorCell_Edited (object o, Gtk.EditedArgs args)
		{

			TreePath path = new TreePath (args.Path);
			TreeIter iter;
			Model.GetIter (out iter, path);
			int i = path.Indices[0];

			SegmentationRecord foo;
			try {
				foo = lista[i];
				foo.Color = args.NewText;
			} catch (Exception e) {
				Console.WriteLine (e);
				return;
			}

			Model.SetValue (iter, (int) Column.Color, foo.Color);
		}

		public void AddNewEntry ()
		{
			lista.Add (new SegmentationRecord ());
			RefreshModel ();
		}

		public void DeleteEntry (int i)
		{
			lista.RemoveAt (i);
			RefreshModel ();
		}

		private void RefreshModel()
		{
			Model.Clear ();
			foreach (SegmentationRecord record in lista) {
				AddValue (record);
			}
		}

		public void ClearAll()
		{
			lista = new List<SegmentationRecord> ();
			RefreshModel ();

		}

		enum Column{
			Enabled,
			HFrom,
			HTo,
			Color
		}
	}
}

