﻿using System;

namespace DICOMTools
{
	public class SegmentationRecord
	{

		public Boolean Enabled { get; set; }
		public int HFrom { get; set; }
		public int HTo { get; set; }
		public string Color { get; set; }

		public SegmentationRecord ()
		{
		}
	}
}

