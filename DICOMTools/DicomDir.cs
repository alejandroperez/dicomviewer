﻿using System;
using Dicom;
using Dicom.Media;
using System.Collections.Generic;

namespace DICOMTools
{
	public class DicomDir
	{
		public string ParentPath { get; set; }

		private DicomDirectoryRecord RootRecord { get; set; }

		public string PatientID { get; set; }
		public string PatientName { get; set; }

		public DicomDir (DicomDirectoryRecord root)
		{
			RootRecord = root;
			//Patient Data
			this.PatientID = root.Get<string>(DicomTag.PatientID);
			this.PatientName = root.Get<string> (DicomTag.PatientName);
				
		}

		public List<SerieDicom> GetSeries(){
			var study = RootRecord.LowerLevelDirectoryRecord;
			var series = study.LowerLevelDirectoryRecordCollection;

			var salida = new List<SerieDicom>();

			foreach (DicomDirectoryRecord serie in series) {			
				List<string> images = new List<string> ();
				foreach (DicomDirectoryRecord element in serie.LowerLevelDirectoryRecordCollection) {
					var imageId = element.Get<string> (DicomTag.ReferencedFileID,-1);
					images.Add (imageId);
					Console.WriteLine (imageId);
				}
				var nuevaSerie = new SerieDicom ();
				nuevaSerie.Id = serie.Get<string>(DicomTag.SeriesInstanceUID);
				nuevaSerie.Images = images;
				salida.Add (nuevaSerie);
			}
			return salida;
		}
			
		public override string ToString(){
			return "Patient: " + this.PatientID + " "
				+ '\n'+ "Name: " + this.PatientName;
		}
	}
}

