﻿using System;
using Dicom;
using Dicom.Media;
using Dicom.Imaging;
using Dicom.Imaging.Codec;
using Dicom.Imaging.Render;
using System.Drawing;
using System.Drawing.Imaging;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace DICOMTools
{
	public class DicomProcessor
	{
		//Save as JPG with custom PhotometricInterpretation
		public static void SaveDCMAsJPG(string fileName)
		{
			DicomDataset dataset = DicomFile.Open(fileName).Dataset;
			//dataset.Set(DicomTag.WindowWidth, 200.0); //the WindowWidth must be non-zero
			dataset.Add(DicomTag.WindowCenter, "100.0");
			//dataset.Add(DicomTag.PhotometricInterpretation, "MONOCHROME1"); //ValueRepresentations tag is broken
			dataset.Add(new DicomCodeString(DicomTag.PhotometricInterpretation, PhotometricInterpretation.Monochrome2.Value));
			DicomImage image = new DicomImage(dataset);
			image.RenderImage().Save(@"test.jpg");
		}


		public static void testProc()
		{
			//DOC
			//http://www.nudoq.org/#!/Packages/fo-dicom/Dicom/DicomPixelData/M/Create

			var image = new DicomImage(@"sample");
			var file = new DicomFile (image.Dataset);

			//Uncompressed Dataset
			var newFile = file.ChangeTransferSyntax (DicomTransferSyntax.ImplicitVRLittleEndian);
			var uncompressed = new DicomImage (newFile.Dataset);

			var pixelData = PixelDataFactory.Create (DicomPixelData.Create (uncompressed.Dataset),0);

			int[] pixelValues = new int[pixelData.Height*pixelData.Width];
			pixelData.Render (Dicom.Imaging.LUT.VOILinearLUT.Create(GrayscaleRenderOptions.FromMinMax(uncompressed.Dataset)), pixelValues);

			var bitmap = new Bitmap (pixelData.Width, pixelData.Height);
			for (int x = 0; x < bitmap.Width; x++) {
				for (int y = 0; y < bitmap.Height; y++) {
					int luminosity = pixelValues[y*bitmap.Width+x];
					bitmap.SetPixel (x, y, Color.FromArgb (luminosity, luminosity, luminosity));
				}
			}
			bitmap.Save (@"salida.jpg");
		}

		public static Bitmap RenderDicom(string fileName){
			var image = new DicomImage(fileName);
			var file = new DicomFile (image.Dataset);

			//Uncompressed Dataset
			var newFile = file.ChangeTransferSyntax (DicomTransferSyntax.ImplicitVRLittleEndian);
			var uncompressed = new DicomImage (newFile.Dataset);

			var pixelData = PixelDataFactory.Create (DicomPixelData.Create (uncompressed.Dataset),0);

			int[] pixelValues = new int[pixelData.Height*pixelData.Width];
			pixelData.Render (Dicom.Imaging.LUT.VOILinearLUT.Create(GrayscaleRenderOptions.FromMinMax(uncompressed.Dataset)), pixelValues);

			var bitmap = new Bitmap (pixelData.Width, pixelData.Height);
			for (int x = 0; x < bitmap.Width; x++) {
				for (int y = 0; y < bitmap.Height; y++) {
					int luminosity = pixelValues[y*bitmap.Width+x];
					bitmap.SetPixel (x, y, Color.FromArgb (luminosity, luminosity, luminosity));
				}
			}
			return bitmap;
		}

		public static Bitmap RenderDicom(string fileName, List<SegmentationRecord> records){
			var image = new DicomImage(fileName);
			var file = new DicomFile (image.Dataset);

			//Uncompressed Dataset
			var newFile = file.ChangeTransferSyntax (DicomTransferSyntax.ImplicitVRLittleEndian);
			var uncompressed = new DicomImage (newFile.Dataset);

			var pixelData = PixelDataFactory.Create (DicomPixelData.Create (uncompressed.Dataset),0);

			int[] pixelValues = new int[pixelData.Height*pixelData.Width];
			pixelData.Render (Dicom.Imaging.LUT.VOILinearLUT.Create(GrayscaleRenderOptions.FromImagePixelValueTags(uncompressed.Dataset)), pixelValues);

			var bitmap = new Bitmap (pixelData.Width, pixelData.Height);
			for (int x = 0; x < bitmap.Width; x++) {
				for (int y = 0; y < bitmap.Height; y++) {
					Color? color = null;
					foreach (var record in records) {
						if (record.Enabled) {
							int value = Convert.ToInt32 (pixelData.GetPixel (x, y));
							if (value > record.HFrom && value < record.HTo) {
								color = (Color)new ColorConverter ().ConvertFromString (record.Color);
							}
						} 
					}
					int luminosity = pixelValues [y * bitmap.Width + x];
					if (!color.HasValue) {
						color = Color.FromArgb (luminosity, luminosity, luminosity);
						luminosity = 1;
					}
					Color pixelColor = Color.FromArgb ((int)luminosity, color.Value);
					bitmap.SetPixel (x, y, pixelColor);
				}
			}
			return bitmap;
		}

		public static DicomDir ParseDICOMDIR(string fileName){
			var dirFile = DicomDirectory.Open (fileName);
			DicomDir dicomdir = new DicomDir (dirFile.RootDirectoryRecord);
			dicomdir.ParentPath = new FileInfo (fileName).Directory.FullName;
			Console.WriteLine ("DICOMDIR:");
			Console.WriteLine (dicomdir.ToString());
			var salida = new List<string>{ };
			return dicomdir;
		}

	}

}

